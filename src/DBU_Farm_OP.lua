local Rayfield = loadstring(game:HttpGet('https://sirius.menu/rayfield'))()

local Window = Rayfield:CreateWindow({
   Name = "Simply DBU | By swuY",
   LoadingTitle = "Loading Simply DBU",
   LoadingSubtitle = "by swuY",
   ConfigurationSaving = {
      Enabled = true,
      FolderName = nil, -- Create a custom folder for your hub/game
      FileName = "SDBU_Setting"
   },
   KeySystem = false, -- Set this to true to use our key system
})


-- Tabs
local mainTab = Window:CreateTab("Home", nil) -- Title, Image
local playersInfoTab = Window:CreateTab("Players Information", nil)


mainTab:CreateSection("Main", nil)
local toggleAutoFarm = mainTab:CreateToggle({
   Name = "Auto Farm",
   CurrentValue = false,
   Flag = "toggleAutoFarm",
   Callback = function(Value)
      -- parameter Value -> true, false
   end,
})

local toggleAutoRebirth = mainTab:CreateToggle({
   Name = "Auto Rebirth",
   CurrentValue = false,
   Flag = "toggleAutoRebirth",
   Callback = function(Value)
      -- parameter Value -> true, false
   end,
})


mainTab:CreateSection("Form Zone", nil)
local Dropdown = mainTab:CreateDropdown({
   Name = "Dropdown Example",
   Options = {"Option 1","Option 2"},
   CurrentOption = {"Option 1"},
   MultipleOptions = false,
   Flag = "Dropdown1",
   Callback = function(Option)
      -- get value of list Options
   end,
})

local dropdownEquipForm = mainTab:CreateDropdown({
   Name = "Equip Form",
   Options = {"Option 1","Option 2"},
   CurrentOption = {"Option 1"},
   MultipleOptions = false,
   Flag = "dropdownEquipForm",
   Callback = function(Option)
      -- get value of list Options
   end,
})

local toggleAutoStackForm = mainTab:CreateToggle({
   Name = "Auto Stack Form",
   CurrentValue = false,
   Flag = "toggleAutoStackForm",
   Callback = function(Value)
      -- parameter Value -> true, false
   end,
})





mainTab:CreateSection("", nil)
local destroyWindowButton = mainTab:CreateButton({
   Name = "Button destroy window",
   Interact = 'Changable',
   Callback = function()
      Rayfield:Destroy()
   end,
})
